import { Injectable } from "@angular/core";
import { BehaviorSubject, Subject } from "rxjs";
import { TokenService } from "../token/token-service";
import { User } from "./user";
import jwt_decode from 'jwt-decode';


@Injectable({ providedIn:'root'})

export class UserService {

    private userSubject: BehaviorSubject<User | null>= new BehaviorSubject<User| null>(null);
    private userName!: string;
    constructor(private tokenService: TokenService){

        this.tokenService.hasToken() && this.decodeAndNotify();
    }

    setToken(token: any){
        this.tokenService.setToken(token);
        this.decodeAndNotify()
    }

    private decodeAndNotify() {
        const token: any= this.tokenService.getToken();
        const user = jwt_decode(token) as User;

        this.userName = user.name;
        this.userSubject.next(user);
        console.log(user)
    }
    getUser() {
        return this.userSubject.asObservable();
    }

    logout() {
        this.tokenService.removeToken()
        this.userSubject.next(null);
    }

    isLogged() {
        return this.tokenService.hasToken()
    }
    getUserName() {
        return this.userName;
    }
}