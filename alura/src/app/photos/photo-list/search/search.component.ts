import { Component, EventEmitter, Input, Output } from "@angular/core";
import { Subject } from "rxjs";
import { debounceTime } from 'rxjs/operators';

@Component({
    selector: 'ap-search',
    templateUrl: './search.component.html'
})
export class SearchComponent { 

    @Output() onTyping: EventEmitter<string> = new EventEmitter<string>();
    @Input() value: string = '';
    debounce: Subject<string> = new Subject<string>();
    ngOnInit(){
        this.debounce
        .pipe(debounceTime(300))
        .subscribe(filter =>this.onTyping.emit(filter))
    }
    ngOnDestroy(): void {
        this.debounce.unsubscribe();
    }
    onKeyUp(target : any) {
        if(target instanceof EventTarget) {
          var elemento = target as HTMLInputElement;
          return elemento.value;
    
        } else {
          return
        }
      }  
 }

